export const state = () => ({
  plan_headers: null,
  plans: {
    home: {}
  }
});

export const getters = {
  getPlan: state => ({ plan_id }) => {
    return state.plans[plan_id];
  },
  getSubjectList: state => ({ plan_id, semester }) => {
    if (!state.plans || !state.plans[plan_id]) {
      if (plan_id == 'home') return [];
      return null;
    }

    return state.plans[plan_id][semester.toString()];
  },
  getPlanHeaders(state) {
    return state.plan_headers;
  },
  getNextAvailablePlan(state) {
    var max = -1;
    for (let plan of state.plan_headers) max = Math.max(max, plan.numero_plano);
    return Math.ceil(max) + 1;
  },
  getHomePlannedCredits: (state, getters, rootState, rootGetters) => type => {
    var total = 0;
    for (let semester in state.plans['home']) {
      for (let subject of state.plans['home'][semester]) {
        subject = rootGetters['subject/getSubject'](subject.id_disciplina);
        let subject_state = rootGetters['subject/getSubjectState'](
          subject.id_disciplina
        );

        if (subject_state != 'done' && subject.tipo == type) {
          total += subject.creditos_trabalho + subject.creditos_aula;
        }
      }
    }
    return total;
  }
};

export const mutations = {
  addSubject(state, { plan_id, subject_id, semester }) {
    let new_plans = { ...state.plans };

    if (!new_plans[plan_id]) new_plans[plan_id] = {};
    if (!new_plans[plan_id][semester.toString()])
      new_plans[plan_id][semester.toString()] = [];

    for (let semester in new_plans[plan_id])
      for (let subject of new_plans[plan_id][semester])
        if (subject.id_disciplina == subject_id) return;

    new_plans[plan_id][semester.toString()].push({
      id_disciplina: subject_id
    });
    state.plans = new_plans;
  },
  setSemester(state, { plan_id, semester, data }) {
    let new_plans = { ...state.plans };
    if (!new_plans[plan_id]) new_plans[plan_id] = {};
    if (!new_plans[plan_id][semester.toString()])
      new_plans[plan_id][semester.toString()] = [];
    new_plans[plan_id][semester.toString()] = data;
    state.plans = new_plans;
  },
  setPlan(state, { plan_id, data }) {
    console.log(state);
    let new_plans = { ...state.plans };
    new_plans[plan_id] = data;
    state.plans = new_plans;
    console.log(state);
  },
  setPlanHeaders(state, { headers }) {
    state.plan_headers = headers;
  }
};

export const actions = {
  async save(context, { plan_id }) {
    let person_id = context.rootGetters['auth/getID'];
    let person_nusp = context.rootGetters['auth/getNUSP'];

    let plan = context.getters.getPlan({ plan_id });
    let plan_number = context.getters.getNextAvailablePlan;

    try {
      for (let semester in plan)
        for (let subject of plan[semester]) {
          await this.$axios.$post('alunos_planejam_disciplinas', {
            id_pessoa: person_id,
            nusp_aluno: person_nusp,
            id_disciplina: subject.id_disciplina,
            ano_semestre: semester,
            numero_plano: plan_number
          });
        }
    } catch (e) {
      console.error(e);
    }

    context.dispatch('loadPlanHeaders');
    context.commit('setPlan', { plan_id: 'home', data: {} });
  },
  async loadSemesterOfPlan(context, { plan_id, semester }) {
    if (plan_id == 'home') return;
    context.commit('setSemester', {
      plan_id,
      semester,
      data: await this.$axios.$post('rpc/disciplinas_plano', {
        id_aluno: context.rootGetters['auth/getID'],
        num_plan: plan_id.split('_')[1],
        semestre: semester
      })
    });
  },
  async loadPlanHeaders(context) {
    context.commit('setPlanHeaders', {
      headers: await this.$axios.$post('rpc/planos', {
        id_pessoa: context.rootGetters['auth/getID']
      })
    });
  }
};

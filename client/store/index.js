// So some of the errors disappear
export const strict = false;

// Add Vuex Persistence plugin
import VuexPersistence from 'vuex-persist';

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
});

export const plugins = [vuexLocal.plugin];

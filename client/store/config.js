export const state = () => ({
  number_of_semesters: 8
});

export const getters = {
  getNumberOfSemesters(state) {
    return state.number_of_semesters;
  }
};

export const mutations = {
  incrementSemestersNumber(state) {
    state.number_of_semesters++;
  }
};

export const actions = {};

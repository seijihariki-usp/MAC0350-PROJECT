export default {
  // Timeout for re-fetching data
  // Current timeout set to: 1 day
  getTimeoutDate() {
    return new Date(new Date().getDate() + 1);
  }
};

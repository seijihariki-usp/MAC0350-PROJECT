export const state = () => ({
  subjects: {},
  subject_states: {}
});

export const getters = {
  getSubject: state => subject_id => {
    if (subject_id in state.subjects) return state.subjects[subject_id];
    return null;
  },
  getSubjectState: state => subject_id => {
    if (subject_id in state.subject_states)
      return state.subject_states[subject_id];
    return null;
  }
};

export const mutations = {
  setSubject(state, { id, subject }) {
    let new_subjects = { ...state.subjects };
    new_subjects[id] = subject;
    state.subjects = new_subjects;
  },
  setSubjectState(state, { id, subject_state }) {
    let new_subject_states = { ...state.subject_states };
    new_subject_states[id] = subject_state;
    state.subject_states = new_subject_states;
  }
};

export const actions = {
  async loadSubjectState(context, { subject_id }) {
    let person_id = context.rootGetters['auth/getID'];
    var result;
    try {
      result = await this.$axios.$post('rpc/subject_state', {
        person_id,
        subject_id
      });
    } catch (e) {
      console.error(e);
      return;
    }

    let subject_state;
    if (result.length == 0 || result[0].nota < 5) subject_state = 'available';
    else if (!result[0].nota) subject_state = 'doing';
    else if (result[0].nota >= 5) subject_state = 'done';

    context.commit('setSubjectState', { id: subject_id, subject_state });
  },
  async loadSubject(context, id) {
    console.log(`hey guys ${id}`);
    var result;
    try {
      result = await this.$axios.$get('/disciplinas', {
        params: { id_disciplina: `eq.${id}` }
      });

      let compulsory_result = await this.$axios.$get('/grade_obrigatoria', {
        params: { id_disciplina: `eq.${id}` }
      });

      if (compulsory_result.length > 0) result[0].tipo = 'obrigatoria';
      else {
        let optional_result = await this.$axios.$get('/grade_optativa', {
          params: { id_disciplina: `eq.${id}` }
        });
        if (optional_result.length > 0) {
          if (optional_result[0].eletiva) result[0].tipo = 'eletiva';
          else result[0].tipo = 'livre';
        }
      }
    } catch (e) {
      console.log(e);
    }

    context.commit('setSubject', { id, subject: result[0] });
  }
};

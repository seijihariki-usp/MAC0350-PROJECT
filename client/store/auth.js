import { getTimeoutDate } from '@/store/store_settings.js';

// Decodes JWT body
let decodeJWT = token => {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(window.atob(base64));
};

// User's email and authenticity token
export const state = () => ({
  email: '',
  token: '',
  person: null
});

export const getters = {
  isAuthenticated(state) {
    return state.token !== '';
  },
  getToken(state) {
    return state.token;
  },
  getEmail(state) {
    return state.email;
  },
  getUUID(state) {
    return decodeJWT(state.token).id_usuario;
  },
  getName(state) {
    if (state.person) return state.person.nome;
  },
  getID(state) {
    if (state.person) return state.person.id_pessoa;
  },
  getIngress(state) {
    if (state.person) return state.person.turma_ingresso;
  },
  getNUSP(state) {
    if (state.person) return state.person.nusp_aluno;
  }
};

export const mutations = {
  setEmail(state, { email }) {
    state.email = email;
  },
  setToken(state, { token }) {
    state.token = token;
  },
  setPerson(state, { person }) {
    state.person = person;
  }
};

export const actions = {
  // Authenticate using email and password
  async authenticate(context, { email, password }) {
    // Request authentication
    var token = '';
    try {
      token = await this.$axios.$post('rpc/autenticar', {
        email,
        senha: password
      });
    } catch (e) {
      console.error(e);
      return e;
    }

    // Save token at global vuex store to be used by auth middleware
    context.commit('setToken', { token });
    context.commit('setEmail', { email });

    // Fetch user data
    var result = null;
    try {
      result = await this.$axios.$post('rpc/informacao_usuario', {
        uuid: context.getters.getUUID
      });
    } catch (e) {
      console.error(e);
      return null;
    }

    // Save person
    context.commit('setPerson', {
      person: result[0]
    });
  },

  // Forget authentication token for this user
  deauthenticate(context) {
    // Erase token and email from store
    context.commit('setToken', { token: '' });
    context.commit('setEmail', { email: '' });
    context.commit('setPerson', { person: null });
  }
};

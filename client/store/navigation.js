export const state = () => ({
  page: 'home',
  semester: 'none'
});

export const getters = {
  getRoute(state) {
    return $nuxt.$route.name;
  },
  getPage(state) {
    return state.page;
  },
  getSemester(state) {
    return state.semester;
  }
};

export const mutations = {
  setPage(state, page) {
    state.page = page;
  },
  setSemester(state, semester) {
    state.semester = semester;
  }
};

export const actions = {
  async navigate(context, { path }) {
    // Split path
    let route = path.split('/')[0];
    let page = path.split('/')[1];

    $nuxt.$router.push('/'+route);
    context.commit('setPage', page);
  }
};

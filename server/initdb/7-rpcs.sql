-------------------------------------------------------------------------------
-- User related rpcs
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- 
-------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
  public.cria_aluno(name varchar, 
  cpf varchar, 
  birth date,
  nusp numeric,
  ingress date,
  email varchar,
  password varchar)
  RETURNS integer 
  LANGUAGE SQL
AS $$
  INSERT INTO pessoas (cpf, nome, nascimento) 
  values (cpf, name, birth);
  INSERT INTO alunos (id_pessoa, nusp_aluno, turma_ingresso) 
  values (
    (SELECT id_pessoa FROM pessoas WHERE pessoas.cpf = cpf),
    nusp,
    ingress
  );
  INSERT INTO auth.usuarios (email_usuario, senha, expira)
  values (email, password, now() + 1 year);
  INSERT INTO pessoas_geram_usuarios (id_pessoa, id_usuario)
  VALUES (
    (SELECT id_pessoa FROM pessoas WHERE pessoas.cpf = cpf),
    (select id_usuario from auth.usuarios WHERE email_usuario = email)
  ) RETURNING 1;
$$;
-------------------------------------------------------------------------------
-- Get al sorts of credit related information
-------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION
  public.creditos_obrigatorios_plano(id_aluno bigint, num_plan numeric)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT sum(creditos_aula + creditos_trabalho)
  FROM (
    select id_disciplina, creditos_aula, creditos_trabalho 
    FROM alunos_planejam_disciplinas 
    JOIN disciplinas USING(id_disciplina) 
    JOIN grade_obrigatoria using(id_disciplina)
    WHERE numero_plano = num_plan 
    AND id_pessoa = id_aluno
    EXCEPT (
      select id_disciplina, creditos_aula, creditos_trabalho 
      from alunos_cursam_disciplinas 
      join professores_oferecem_disciplinas using (id_oferecimento) 
      join disciplinas using (id_disciplina) 
      where alunos_cursam_disciplinas.id_pessoa = id_aluno 
      and nota >= 5)
  ) AS MATERIAS;
$$;

CREATE OR REPLACE FUNCTION
  public.creditos_eletivos_plano(id_aluno bigint, num_plan numeric)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT sum(creditos_aula + creditos_trabalho)
  FROM (
    select id_disciplina, creditos_aula, creditos_trabalho 
    FROM alunos_planejam_disciplinas 
    JOIN disciplinas USING(id_disciplina) 
    JOIN grade_optativa using(id_disciplina)
    WHERE numero_plano = num_plan 
    AND eletiva IS true
    AND id_pessoa = id_aluno
    EXCEPT (
      select id_disciplina, creditos_aula, creditos_trabalho 
      from alunos_cursam_disciplinas 
      join professores_oferecem_disciplinas using (id_oferecimento) 
      join disciplinas using (id_disciplina) 
      where alunos_cursam_disciplinas.id_pessoa = id_aluno 
      and nota >= 5)
  ) AS MATERIAS;
$$;

CREATE OR REPLACE FUNCTION
  public.creditos_livres_plano(id_aluno bigint, num_plan numeric)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT sum(creditos_aula + creditos_trabalho)
  FROM (
    select id_disciplina, creditos_aula, creditos_trabalho 
    FROM alunos_planejam_disciplinas 
    JOIN disciplinas USING(id_disciplina) 
    JOIN grade_optativa using(id_disciplina)
    WHERE numero_plano = num_plan 
    AND eletiva IS false
    AND id_pessoa = id_aluno
    EXCEPT (
      select id_disciplina, creditos_aula, creditos_trabalho 
      from alunos_cursam_disciplinas 
      join professores_oferecem_disciplinas using (id_oferecimento) 
      join disciplinas using (id_disciplina) 
      where alunos_cursam_disciplinas.id_pessoa = id_aluno 
      and nota >= 5)
  ) AS MATERIAS;
$$;

CREATE OR REPLACE FUNCTION
  public.total_creditos_obrigatorios(id_aluno bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT sum(creditos_trabalho + creditos_aula) 
  FROM disciplinas 
  JOIN grade_obrigatoria 
  USING (id_disciplina);
$$;

-- Get student credits
CREATE OR REPLACE FUNCTION
  public.creditos_obrigatorios(id_aluno bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT sum(creditos_aula + creditos_trabalho) 
  FROM alunos_cursam_disciplinas 
  JOIN alunos USING (nusp_aluno) 
  JOIN professores_oferecem_disciplinas USING (id_oferecimento) 
  JOIN disciplinas USING (id_disciplina) 
  JOIN grade_obrigatoria USING (id_disciplina)
  WHERE nota IS NOT NULL
  AND nota >= 5
  AND alunos.id_pessoa = id_aluno;
$$;

CREATE OR REPLACE FUNCTION
  public.creditos_livres(id_aluno bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT sum(creditos_aula + creditos_trabalho) AS creditos 
  FROM alunos_cursam_disciplinas 
  JOIN alunos USING (nusp_aluno) 
  JOIN professores_oferecem_disciplinas USING (id_oferecimento) 
  JOIN disciplinas USING (id_disciplina) 
  JOIN grade_optativa USING (id_disciplina) 
  WHERE nota IS NOT NULL 
  AND nota >= 5
  AND eletiva IS false
  AND alunos.id_pessoa = id_aluno;
$$;

CREATE OR REPLACE FUNCTION
  public.creditos_eletivos(id_aluno bigint)
  RETURNS bigint
  LANGUAGE SQL
AS $$
  SELECT sum(creditos_aula + creditos_trabalho) AS creditos 
  FROM alunos_cursam_disciplinas 
  JOIN alunos USING (nusp_aluno) 
  JOIN professores_oferecem_disciplinas USING (id_oferecimento) 
  JOIN disciplinas USING (id_disciplina) 
  JOIN grade_optativa USING (id_disciplina) 
  WHERE nota IS NOT NULL 
  AND nota >= 5
  AND eletiva IS true
  AND alunos.id_pessoa = id_aluno;
$$;
-------------------------------------------------------------------------------
-- End credit related stuff
-------------------------------------------------------------------------------

-- Get all subjects from a plan
CREATE OR REPLACE FUNCTION
  public.disciplinas_plano(id_aluno bigint, num_plan numeric, semestre numeric(5,1))
  RETURNS TABLE (
    id_disciplina bigint,
    codigo_disciplina character(7),
    nome varchar(280)
  )
  LANGUAGE SQL
AS $$
  SELECT id_disciplina, codigo_disciplina, nome 
  FROM alunos_planejam_disciplinas 
  JOIN disciplinas USING(id_disciplina) 
  WHERE numero_plano = num_plan 
  AND ano_semestre = semestre
  AND id_pessoa = id_aluno; 
$$;

-- Get all numbers of plans
CREATE OR REPLACE FUNCTION
  public.planos(id_pessoa numeric)
  RETURNS TABLE (
    numero_plano bigint
  )
  LANGUAGE SQL
AS $$
  SELECT DISTINCT numero_plano FROM alunos_planejam_disciplinas WHERE id_pessoa = id_pessoa;
$$;

-- Get all user data from UUID
CREATE OR REPLACE FUNCTION
  public.informacao_usuario(uuid UUID)
  RETURNS TABLE (
      id_pessoa bigint,
      cpf character(11),
      nome varchar(280),
      nascimento date,
      id_usuario UUID,
      ultimo_login date,
      nusp_aluno varchar(10),
      turma_ingresso date,
      nusp_professor varchar(10),
      departamento varchar(100),
      sala varchar(20)
  )
  LANGUAGE SQL
AS $$
  SELECT * FROM pessoas JOIN pessoas_geram_usuarios USING (id_pessoa)
    LEFT JOIN alunos USING (id_pessoa) LEFT JOIN professores USING (id_pessoa)
    WHERE id_usuario = uuid LIMIT 1;
$$;

-------------------------------------------------------------------------------
-- Subject related rpcs
-------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION
  public.subject_state(person_id bigint, subject_id bigint)
  RETURNS TABLE (
      id_pessoa bigint,
      nota numeric,
      id_disciplina bigint
  )
  LANGUAGE SQL
AS $$
  SELECT alunos_cursam_disciplinas.id_pessoa, nota, id_disciplina
    FROM alunos_cursam_disciplinas JOIN professores_oferecem_disciplinas USING (id_oferecimento) WHERE
    alunos_cursam_disciplinas.id_pessoa = person_id AND id_disciplina = subject_id ORDER BY nota DESC NULLS FIRST LIMIT 1;
$$;